;;; Code:

;;auctex
(unless (package-installed-p 'auctex)
  (package-install 'auctex))

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

;; Forward and Inverse Search
(setq TeX-PDF-mode t)
(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-method 'synctex)
(setq TeX-source-correlate-start-server t)

;;cdlatex
(unless (package-installed-p 'cdlatex)
  (package-install 'cdlatex))
(add-hook 'LaTeX-mode-hook #'turn-on-cdlatex)   ; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook #'turn-on-cdlatex)   ; with Emacs latex mode
(add-hook 'LaTeX-mode-hook #'turn-on-reftex)   ; with AUCTeX RefTex mode

;; pdf-tools  pdf-tools-install -> epdfinfo
;; (unless (package-installed-p 'pdf-tools)
;;   (package-install 'pdf-tools))
;;(pdf-tools-install)
;;(pdf-loader-install)
;; (add-hook 'pdf-view-mode-hook 'pdf-view-fit-width-to-window)

;; Okular
;;(setq TeX-view-program-list '(("Okular" "okular --unique %u")))
(setq TeX-view-program-selection '((output-pdf "Okular")))
(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)

;; Envince
;; (setq TeX-view-program-list '(("Evince" "evince --page-index=%(outpage) %o")))
;; (setq TeX-view-program-selection '((output-pdf "Evince")))

;; prettyfi-symbols-mode
;; (add-hook 'LaTeX-mode-hook #'prettify-symbols-mode)
;; (setq prettify-symbols-unprettify-at-point t)
;;(set-fontset-font "fontset-default" 'mathematical "Cambria Math")

;; outline-minor-mode
(add-hook 'LaTeX-mode-hook #'outline-minor-mode)
(add-hook 'latex-mode-hook #'outline-minor-mode)

(add-hook 'LaTeX-mode-hook #'TeX-fold-mode)

(provide 'init-latex)
;;; init-latex.el ends here
