;;; Code:

(setq org-modules-loaded t)

;; org-modern
(unless (package-installed-p 'org-modern)
  (package-install 'org-modern))
(add-hook 'org-mode-hook 'org-modern-mode)
(add-hook 'org-agenda-finalize-hook #'org-modern-agenda)

(setq
 ;; Edit settings
 org-auto-align-tags nil
 org-tags-column 0
 org-catch-invisible-edits 'show-and-error
 org-special-ctrl-a/e t
 org-insert-heading-respect-content t

 ;; Org styling, hide markup etc.
 org-hide-emphasis-markers t
 org-pretty-entities t

 ;; Agenda styling
 org-agenda-tags-column 0
 org-agenda-block-separator ?─
 org-agenda-time-grid
 '((daily today require-timed)
   (800 1000 1200 1400 1600 1800 2000)
   " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
 org-agenda-current-time-string
 "◀── now ─────────────────────────────────────────────────")



;;(with-eval-after-load 'org (global-org-modern-mode))

(provide 'init-org)

;;; init-org.el ends here
