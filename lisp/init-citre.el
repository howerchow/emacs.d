;;; package --- Summary
;;; Comm
;;; Code:
(unless (package-installed-p 'citre)
  (package-install 'citre))
(require 'citre)
(require 'citre-config)
 ;; in `citre-mode-map' so you can only use them when `citre-mode' is enabled.
;; (global-set-key (kbd "C-x c j") 'citre-jump)
;; (global-set-key (kbd "C-x c J") 'citre-jump-back)
;; (global-set-key (kbd "C-x c p") 'citre-peek)
;; (global-set-key (kbd "C-x c u") 'citre-update-this-tags-file)

(keymap-global-set "C-x c j" 'citre-jump)
(keymap-global-set "C-x c J" 'citre-jump-back)
(keymap-global-set "C-x c p" 'citre-peek)
(keymap-global-set "C-x c u" 'citre-update-this-tags-file)

(setq citre-enable-imenu-integration nil)
(setq citre-peek-fill-fringe nil)
;;(setq citre-peek-use-dashes-as-horizontal-border t)
;;(add-hook 'find-file-hook #'citre-auto-enable-citre-mode)

(provide 'init-citre)
;;
