 ;;; Code:

;;yasnippet
(unless (package-installed-p 'yasnippet)
  (package-install 'yasnippet))
(require 'yasnippet)
(yas-global-mode 1)

(unless (package-installed-p 'yasnippet-snippets)
  (package-install 'yasnippet-snippets))
(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)
(add-hook 'treesit-auto-mode-hook #'yas-minor-mode)
(add-hook 'c-ts-mode-hook #'yas-minor-mode)
(add-hook 'c++-ts-mode-hook #'yas-minor-mode)
;; ;;flycheck
;; (unless (package-installed-p 'flycheck)
;;   (package-install 'flycheck))
;; ;; ;;(global-flycheck-mode)
;; (add-hook 'c-mode-hook #'flycheck-mode)
;; (add-hook 'c++-mode-hook
;; 	  (lambda () (setq flycheck-clang-language-standard "c++23")))

;;magit
(unless (package-installed-p 'magit)
  (package-install 'magit))

(setq package-install-upgrade-built-in t)
;; (unless (package-installed-p 'transient)
;;   (package-install 'transient))

;;smartparens
(unless (package-installed-p 'smartparens)
  (package-install 'smartparens))
;;(require 'smartparens-config)
(add-hook 'prog-mode-hook #'smartparens-mode)

;;visual-line-mode
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)
(add-hook 'markdown-mode-hook 'turn-on-visual-line-mode)
(add-hook 'org-mode-hook 'turn-on-visual-line-mode)
(add-hook 'eww-mode-hook 'turn-on-visual-line-mode)

;;markdown
(unless (package-installed-p 'markdown-mode)
  (package-install 'markdown-mode))
(setq markdown-fontify-code-blocks-natively t)


;;hungry-delete
(unless (package-installed-p 'hungry-delete)
  (package-install 'hungry-delete))
;;(require 'hungry-delete)
(add-hook 'prog-mode-hook #'hungry-delete-mode)
(add-hook 'LaTeX-mode-hook #'hungry-delete-mode)

;;cmake-mode
(unless (package-installed-p 'cmake-mode)
  (package-install 'cmake-mode))
(require 'cmake-mode)
(setq cmake-tab-width 4)

;; graphviz-dot-mode
(unless (package-installed-p 'graphviz-dot-mode)
  (package-install 'graphviz-dot-mode))
(require 'graphviz-dot-mode)

;;corfu
(unless (package-installed-p 'corfu)
  (package-install 'corfu))
(require 'corfu)
(global-corfu-mode)
;;Enable auto completion and configure quitting
(setq corfu-cycle t                ;; Enable cycling for `corfu-next/previous'
      corfu-auto t                 ;; Enable auto completion
      corfu-quit-at-boundary t     ;; Automatically quit at word boundary
      corfu-quit-no-match t        ;; Automatically quit if there is no match
      corfu-preview-current nil    ;; Disable current candidate preview
      corfu-echo-documentation nil ;; Disable documentation in the echo area
      corfu-auto-prefix 3
      corfu-quit-no-match 'separator) ;; or t

;; ;;corfu-popupinfo
;; (add-hook 'corfu-mode-hook #'corfu-popupinfo-mode)
;; (setq corfu-popupinfo-delay 0.1)

;;cape
(unless (package-installed-p 'cape)
  (package-install 'cape))
(add-to-list 'completion-at-point-functions #'cape-file)
(add-to-list 'completion-at-point-functions #'cape-dabbrev)
(add-to-list 'completion-at-point-functions #'cape-history)
(add-to-list 'completion-at-point-functions #'cape-keyword)
;;(add-to-list 'completion-at-point-functions #'cape-tex)
;;(add-to-list 'completion-at-point-functions #'cape-sgml)
;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
(add-to-list 'completion-at-point-functions #'cape-ispell)
;;(add-to-list 'completion-at-point-functions #'cape-dict)
(add-to-list 'completion-at-point-functions #'cape-symbol)
;;(add-to-list 'completion-at-point-functions #'cape-line)
(add-to-list 'completion-at-point-functions #'cape-c)

;;tree-sitter
;; (unless (package-installed-p 'tree-sitter)
;;   (package-install 'tree-sitter))
;; (require 'tree-sitter)

;; (unless (package-installed-p 'tree-sitter-langs)
;;   (package-install 'tree-sitter-langs))
;; (require 'tree-sitter-langs)
;; (global-tree-sitter-mode)
;; ;; (add-hook 'c-mode-hook #'tree-sitter-mode)
;; ;; (add-hook 'c++-mode-hook #'tree-sitter-mode)
;; (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)



;;avy
(unless (package-installed-p 'avy)
  (package-install 'avy))
(require 'avy)
(setopt avy-background t
	avy-style 'at-full
	avy-timeout-seconds 0.2)
(global-set-key (kbd"C-:") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-g f") 'avy-goto-line)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)
(global-set-key (kbd "M-g e") 'avy-goto-word-0)

;;(avy-setup-default)
;;(global-set-key (kbd "C-c C-j") 'avy-resume)

;;web-mode
(unless (package-installed-p 'web-mode)
  (package-install 'web-mode))
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(with-eval-after-load 'web-mode
  (define-key web-mode-map (kbd "C-c C-v") 'browse-url-of-buffer))

;;emment
(unless (package-installed-p 'emmet-mode)
  (package-install 'emmet-mode))
;;(require 'emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode)
(add-hook 'html-mode-hook 'emmet-mode)
(add-hook 'css-mode-hook  'emmet-mode)


;;expand-region
(unless (package-installed-p 'expand-region)
  (package-install 'expand-region))
(require 'expand-region)
;;(global-set-key (kbd "C-=") 'er/expand-region)
(keymap-global-set "C-=" 'er/expand-region)

;;
(setq c-default-style "linux")

;;terminal-here
(unless (package-installed-p 'terminal-here)
  (package-install 'terminal-here))
(require 'terminal-here)
(setq terminal-here-linux-terminal-command 'konsole)
(keymap-global-set "C-<f5>" #'terminal-here-launch)
(keymap-global-set "C-<f6>" #'terminal-here-project-launch)

;;ag
(unless (package-installed-p 'ag)
  (package-install 'ag))
(setq ag-highlight-search t)

;; treesit-auto
(unless (package-installed-p 'treesit-auto)
  (package-install 'treesit-auto))
(require 'treesit-auto)
(global-treesit-auto-mode)
(setq treesit-auto-install t
      treesit-font-lock-level 4

      )




;; ;; highlight-thing
;; (unless (package-installed-p 'highlight-thing)
;;   (package-install 'highlight-thing))
;; (require 'highlight-thing)
;; ;;(global-highlight-thing-mode)
;; (add-hook 'prog-mode-hook 'highlight-thing-mode)
;; (add-hook 'LaTeX-mode-hook 'highlight-thing-mode)
;; ;; Don't highlight the thing at point itself. Default is nil.
;; ;;(setq highlight-thing-exclude-thing-under-point t)
;; (setq highlight-thing-all-visible-buffers-p t)

;; bash-completion
(unless (package-installed-p 'bash-completion)
  (package-install 'bash-completion))
(require 'bash-completion)
(bash-completion-setup)

;; lua-mode
(unless (package-installed-p 'lua-mode)
  (package-install 'lua-mode))


;; yaml-mode
(unless (package-installed-p 'yaml-mode)
  (package-install 'yaml-mode))
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

;; auto-compile
(unless (package-installed-p 'auto-compile)
  (package-install 'auto-compile))
(require 'auto-compile)
(auto-compile-on-load-mode)
(auto-compile-on-save-mode)

;; doom-modeline
(unless (package-installed-p 'doom-modeline)
  (package-install 'doom-modeline))
(require 'doom-modeline)
(doom-modeline-mode 1)
(setopt doom-modeline-buffer-file-name-style 'relative-from-project
	;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
	doom-modeline-major-mode-icon t
	doom-modeline-major-mode-color-icon t
	doom-modeline-buffer-modification-icon t
	doom-modeline-time-icon t
	doom-modeline-enable-word-count nil
	)
(setq doom-modeline-time-analogue-clock t)
(setq doom-modeline-buffer-name t)
;;(setq doom-modeline-project-name t)

;; super-save
(unless (package-installed-p 'super-save)
  (package-refresh-contents)
  (package-install 'super-save))
;;(super-save-mode 1)


;; clang-format
(unless (package-installed-p 'clang-format)
  (package-refresh-contents)
  (package-install 'clang-format))

;; clang-format-lite
(unless (package-installed-p 'clang-format-lite)
  (package-refresh-contents)
  (package-install 'clang-format-lite))
;; (setq clang-format-lite-only-if-config t) ;; run clang-format only if the .clang-format file is found
;; (add-hook 'c++-mode-hook #'clang-format-lite-save-hook) ;; and we are in c++ mode
;; (add-hook 'c-mode-hook #'clang-format-lite-save-hook) ;; or we are in c mode
;; (add-hook 'c++-ts-mode-hook #'clang-format-lite-save-hook)
;; (add-hook 'c-ts-mode-hook #'clang-format-lite-save-hook)

(add-hook 'c-ts-mode-hook
          (lambda ()
            (add-hook 'before-save-hook 'clang-format-buffer nil t)))


;; prettier-js
(unless (package-installed-p 'prettier-js)
  (package-refresh-contents)
  (package-install 'prettier-js))

;; separedit
(unless (package-installed-p 'separedit)
  (package-refresh-contents)
  (package-install 'separedit))

;; vterm
(unless (package-installed-p 'vterm)
  (package-refresh-contents)
  (package-install 'vterm))

;; anzu
(unless (package-installed-p 'anzu)
  (package-install 'anzu))
(global-anzu-mode 1)

;; blamer
(unless (package-installed-p 'blamer)
  (package-install 'blamer))
;; (global-blamer-mode 1)
;; (add-hook 'prog-mode-hook #'blamer-mode)
;; (setq blamer-idle-time 0.3
;;       blamer-min-offset 70)

;; keycast
(unless (package-installed-p 'keycast)
  (package-install 'keycast))

;; ;; telephone-line
;; (unless (package-installed-p 'telephone-line)
;;   (package-install 'telephone-line))
;; (require 'telephone-line)
;; (setq telephone-line-primary-left-separator 'telephone-line-cubed-left
;;       telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left
;;       telephone-line-primary-right-separator 'telephone-line-cubed-right
;;       telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
;; (setq telephone-line-height 26
;;       telephone-line-evil-use-short-tag t)
;; (telephone-line-mode 1)

;; line-reminder
;; (unless (package-installed-p 'line-reminder)
;;   (package-install 'line-reminder))
;; (setq line-reminder-show-option 'linum)  ; Or set to 'indicators
;; (setq line-reminder-modified-sign "▐"
;;       line-reminder-saved-sign "▐")
;; (global-line-reminder-mode t)


;; maxima
;; (unless (package-installed-p 'maxima)
;;   (package-install 'maxima))
;;  (autoload 'maxima-mode "maxima" "Maxima mode" t)
;; (autoload 'maxima "maxima" "Maxima interaction")
;; (setq auto-mode-alist (cons '("\\.mac" . maxima-mode) auto-mode-alist))

;; frimacs
(unless (package-installed-p 'frimacs)
  (package-install 'frimacs))

;; vertico
;; (unless (package-installed-p 'vertico)
;;   (package-install 'vertico))
;; (vertico-mode 1)

;; marginalia
;; (unless (package-installed-p 'marginalia)
;;   (package-install 'marginalia))
;; (marginalia-mode 1)

;; consult
;; (unless (package-installed-p 'consult)
;;   (package-install 'consult))

;; orderless
;; (unless (package-installed-p 'orderless)
;;   (package-install 'orderless))
;; (require 'orderless)
;; (setq completion-styles '(orderless basic)
;;       completion-category-overrides '((file (styles basic partial-completion))))


(unless (package-installed-p 'editorconfig)
  (package-install 'editorconfig))
(editorconfig-mode 1)

(unless (package-installed-p 'impatient-mode)
  (package-install 'impatient-mode))
;;(add-hook 'html-mode-hook #'start-h)


;; iedit
(unless (package-installed-p 'iedit)
  (package-install 'iedit))
(require 'iedit)

;; got-chg
(unless (package-installed-p 'goto-chg)
  (package-install 'goto-chg))
(require 'goto-chg)
(global-set-key [(control ?.)] 'goto-last-change)
(global-set-key [(control ?,)] 'goto-last-change-reverse)

;; hideshowvis
;; (unless (package-installed-p 'hideshowvis)
;;   (package-install 'hideshowvis))
;; (add-hook 'prog-mode-hook #'hideshowvis-minor-mode)

;; line-reminder
(unless (package-installed-p 'line-reminder)
  (package-install 'line-reminder))
(add-hook 'prog-mode-hook #'line-reminder-mode)

;; ace-window
(unless (package-installed-p 'ace-window)
  (package-install 'ace-window))
(global-set-key (kbd "C-x o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
(setopt aw-scope 'visible)

;; js2-mode
;;(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; symbol-overlay
(unless (package-installed-p 'symbol-overlay)
  (package-install 'symbol-overlay))
(require 'symbol-overlay)
(global-set-key (kbd "M-i") 'symbol-overlay-put)
(global-set-key (kbd "M-n") 'symbol-overlay-switch-forward)
(global-set-key (kbd "M-p") 'symbol-overlay-switch-backward)
(global-set-key (kbd "<f7>") 'symbol-overlay-mode)
(global-set-key (kbd "<f8>") 'symbol-overlay-remove-all)
;;(add-hook 'prog-mode-hook #'symbol-overlay-mode)

;; nasm-mode
(unless (package-installed-p 'nasm-mode)
  (package-install 'nasm-mode))

;; dogears
(unless (package-installed-p 'dogears)
  (package-install 'dogears))
(dogears-mode t)
(setq dogears-idle 1
      dogears-limit 200
      dogears-position-delta 20 )

;; move-text
(unless (package-installed-p 'move-text)
  (package-install 'move-text))
(move-text-default-bindings)

;; uniline
(unless (package-installed-p 'uniline)
  (package-install 'uniline))


;; nerd-icons-dired
(unless (package-installed-p 'nerd-icons-dired)
  (package-install 'nerd-icons-dired))
;;(require 'nerd-icons-dired)
(add-hook 'dired-mode-hook 'nerd-icons-dired-mode)


;; sideline
(unless (package-installed-p 'sideline)
  (package-install 'sideline))
(require 'sideline)
(setq sideline-backends-left-skip-current-line t   ; don't display on current line (left)
      sideline-backends-right-skip-current-line t  ; don't display on current line (right)
      sideline-order-left 'down                    ; or 'up
      sideline-order-right 'up                     ; or 'down
      sideline-format-left "%s   "                 ; format for left aligment
      sideline-format-right "   %s"                ; format for right aligment
      sideline-priority 100                        ; overlays' priority
      ;;sideline-display-backend-name t
      )            ; display the backend name


;; sideline-flymake
(unless (package-installed-p 'sideline-flymake)
  (package-install 'sideline-flymake))
(require 'sideline-flymake)

(add-hook 'flymake-mode-hook #'sideline-mode)
(setq sideline-flymake-display-mode 'line)
(setq sideline-backends-right '(sideline-flymake))

;;(setq flymake-show-diagnostics-at-end-of-line t)

;; breadcrumb
(unless (package-installed-p 'breadcrumb)
  (package-install 'breadcrumb))

;; tab-line-nerd-icons
(unless (package-installed-p 'tab-line-nerd-icons)
  (package-install 'tab-line-nerd-icons))
(tab-line-nerd-icons-global-mode)

;; visual-replace
(unless (package-installed-p 'visual-replace)
  (package-install 'visual-replace))
(require 'visual-replace)
(visual-replace-global-mode 1)

;; ;; centaur-tabs
;; (unless (package-installed-p 'centaur-tabs)
;;   (package-install 'centaur-tabs))
;; ;;(require 'centaur-tabs)
;; ;;(centaur-tabs-mode t)

;; (setq centaur-tabs-style "wave"
;;       centaur-tabs-set-icons t
;;       centaur-tabs-icon-type 'nerd-icons  ; or 'all-the-icons
;;       ;;(setq centaur-tabs-set-bar 'left)
;;       ;;centaur-tabs-show-navigation-buttons t
;;       ;;centaur-tabs-set-bar 'left
;;       centaur-tabs-show-count nil
;;       centaur-tabs-set-modified-marker t
;;       centaur-tabs-cycle-scope 'tabs)

;; (with-eval-after-load 'centaur-tabs-mode
;;   (global-set-key (kbd "C-<prior>")  'centaur-tabs-backward)
;;   (global-set-key (kbd "C-<next>") 'centaur-tabs-forward)
;;   (global-set-key  (kbd "C-S-<prior>") 'centaur-tabs-move-current-tab-to-left)
;;   (global-set-key  (kbd "C-S-<next>") 'centaur-tabs-move-current-tab-to-right)

;;   (add-hook 'dired-mode-hook #'centaur-tabs-local-mode)
;;   (add-hook 'bookmark-bmenu-mode-hook #'centaur-tabs-local-mode)
;;   (add-hook 'text-mode-hook #'centaur-tabs-local-mode)
;;   (add-hook 'magit-diff-mode-hook #'centaur-tabs-local-mode))


;; benchmark-init
(unless (package-installed-p 'benchmark-init)
  (package-install 'benchmark-init))
(require 'benchmark-init)
;; To disable collection of benchmark data after init is done.
(add-hook 'after-init-hook 'benchmark-init/deactivate)


;; js2-mode
(unless (package-installed-p 'js2-mode)
  (package-install 'js2-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; sticky-scroll-mode
(unless (package-installed-p 'sticky-scroll-mode)
  (package-install 'sticky-scroll-mode))
;;(add-hook 'prog-mode-hook #'sticky-scroll-mode)


;; python-mode
(unless (package-installed-p 'python-mode)
  (package-install 'python-mode))



;; (require 'dired-preview)
;; ;; Default values for demo purposes
;; (setq dired-preview-delay 0.7)
;; (setq dired-preview-max-size (expt 2 20))
;; (setq dired-preview-ignored-extensions-regexp
;;         (concat "\\."
;;                 "\\(gz\\|"
;;                 "zst\\|"
;;                 "tar\\|"
;;                 "xz\\|"
;;                 "rar\\|"
;;                 "zip\\|"
;;                 "iso\\|"
;;                 "epub"
;;                 "\\)"))

;; ;; Enable `dired-preview-mode' in a given Dired buffer or do it
;; ;; globally:
;; (dired-preview-global-mode 1)

(provide 'init-package)
;;
