;;; package --- Summary
;;; Comm
;;; Code:

;; (setq package-archives '(("gnu"   . "http://elpa.emacs-china.org/gnu/")
;;                          ("melpa" . "http://elpa.emacs-china.org/melpa/")
;; 			 ("org" . "http://elpa.emacs-china.org/org/")
;; ;;			 ("marmalade" . "http://elpa.emacs-china.org/marmalade/")
;; 			 ))

;; (setq package-archives
;;     '(("melpa" . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/melpa/")
;;       ("org"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/org/")
;;       ("gnu"   . "https://raw.githubusercontent.com/d12frosted/elpa-mirror/master/gnu/")))

(setq package-archives
      '(
	("melpa" . "https://melpa.org/packages/")
	("elpa" . "https://elpa.gnu.org/packages/")
	;;("org" . "https://orgmode.org/elpa/")
	("gnu" . "https://elpa.gnu.org/packages/")
	;;("gnu-devel" . "https://elpa.gnu.org/devel/")
	("nongnu" . "https://elpa.nongnu.org/nongnu/")
	;;("melpa-stable" . "https://stable.melpa.org/packages/")
	))

;; (setq package-archives '(("gnu"   . "http://1.15.88.122/gnu/")
;;                            ("melpa" . "http://1.15.88.122/melpa/")))

(setq package-check-signature nil)

;;(setq package-quickstart t)

;;(require 'package)
(unless (bound-and-true-p package-initialized)
  (package-initialize))

(unless package-archive-contents
  (package-refresh-contents))

(provide 'init-elpa)
;;with-eval-after-loadyasnippet
