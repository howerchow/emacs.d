
;;; init-builtin.el --- initialize the builtin plugins -*- lexical-binding: t -*-
;;; Commentary:
;; (c) Hower Chow, 2022-

;;; Code:

;; disable menu bar
(menu-bar-mode -1)

;; disable tool bar
(tool-bar-mode -1)

;; disable scrool bar
(scroll-bar-mode -1)

;; global highlight line
(global-hl-line-mode)

;; disable backupfiles
(setq make-backup-files nil
      auto-save-default nil)

;; Improve display
(setq display-raw-bytes-as-hex t
      redisplay-skip-fontification-on-input t)

;; disable ring bell
(setq ring-bell-function 'ignore)

;; disable blink curseor
;;(setq blink-cursor-mode nil)
(setq-default cursor-type 'bar)

;; use short answers for YES/NO ect.
(setq use-short-answers t)

(setq-default fill-column 80)

;; imenu
;;(add-hook 'c-mode-hook 'imenu-add-menubar-index)

;; recentf
(recentf-mode 1)

;; make tab-width always
;; only use spaces instead of TAB, use C-q TAB to input the TAB char
;; (setq-default tab-width 4
;;               indent-tabs-mode nil)

;; auto-fill-mode, Help by command or variable name
(add-hook 'after-init-hook 'auto-fill-mode)

;; auto revert
;; `global-auto-revert-mode' is provided by autorevert.el (builtin)
(add-hook 'after-init-hook 'global-auto-revert-mode)
(setq auto-revert-use-notify nil)
(setopt auto-revert-verbose nil)

;; auto save to the visited file
;;(add-hook 'after-init-hook 'auto-save-visited-mode)
(when (>= emacs-major-version 26)
  ;; real auto save
  (auto-save-visited-mode 1)
  (setq auto-save-visited-interval 30)
  )

;; Delete Behavior
;; `delete-selection-mode' is provided by delsel.el (builtin)
;; `delete-trailing-whitespace' is provided by simple.el (builtin)
(add-hook 'before-save-hook #'delete-trailing-whitespace)


(delete-selection-mode t)


;; Flyspell
;; to use this package, you may install 'aspell' and dict by manual
;; for example, "pacman -S aspell" on archlinux
;; and "pacman -S pacman -S mingw64/mingw-w64-x86_64-aspell{,-en}" on msys2 (Windows)
;; for performance issue, do NOT use on Windows
(add-hook 'text-mode-hook 'flyspell-mode)
;;(add-hook 'prog-mode-hook #'flyspell-prog-mode)

;; (add-hook 'text-mode #'flyspell-mode)
;; (add-hook 'org-mode #'flyspell-mode)


;; ibuffer
;;(defalias 'list-buffers 'ibuffer)

;; iSearch
;; (setq isearch-allow-motion t
;;       isearch-lazy-count t)

;; minibuffer
(add-hook 'after-init-hook 'minibuffer-electric-default-mode)
(setq minibuffer-depth-indicate-mode t
      minibuffer-follows-selected-frame nil)

;; modeline settings
;; column number is useless in most time, but useful when debug code.
;;(add-hook 'after-init-hook 'column-number-mode)
;;(setq mode-line-compact t)

;; global visual line mode
;;(add-hook 'after-init-hook 'global-visual-line-mode)

;; pixel scroll
(if (fboundp 'pixel-scroll-precision-mode)
    (pixel-scroll-precision-mode)
  (pixel-scroll-mode))
;; Pixelwise resize
(setq window-resize-pixelwise t
      frame-resize-pixelwise t)
;; (pixel-scroll-precision-mode 1)
;; (setq pixel-scroll-precision-interpolate-page t)
;; (defalias 'scroll-up-command 'pixel-scroll-interpolate-down)
;; (defalias 'scroll-down-command 'pixel-scroll-interpolate-up)

;;minibuffer case sensitivity
(setq read-buffer-completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)

;; frame title
(setq frame-title-format '("GNU Emacs@" emacs-version))


;; set indentation style linux for c++
;; (setq c-default-style "linux"
;;       c-basic-offset 4)

;; upcase-region
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;;
;;(global-set-key [remap list-buffers] 'ibuffer)


;; Alternatively, to use it only in programming modes:
;;(global-display-line-numbers-mode t)
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'markdown-mode-hook #'display-line-numbers-mode)

;;winner-mode
;;(add-hook 'after-init-hook 'winner-mode)
(winner-mode)


;;delete-selection-mode
;;(add-hook 'after-init-hook 'delete-selection-mode)
(delete-selection-mode)

;;show-paren-mode
(add-hook 'after-init-hook 'show-paren-mode)
(setq show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t
      show-paren-context-when-offscreen t
      show-paren-dela 0)
;;(setq-default show-paren-style 'expression)

;; global-auto-revert-mode
(add-hook 'after-init-hook 'global-auto-revert-mode)

;; so-long
(add-hook 'after-init-hook 'global-so-long-mode)

;; (global-unset-key "\C-z")
(keymap-global-unset "C-z")
;;(keymap-global-unset "C-x C-c")

(setq scroll-preserve-screen-position 'always)

;; dired
(setq dired-dwim-target t)
(setq dired-listing-switches "-alh")
;;(setq dired-listing-switches "-aghvGL")
;;(add-hook 'dired-mode-hook #'dired-hide-details-mode)
(setq delete-by-moving-to-trash t
      load-prefer-newer t
      global-auto-revert-non-file-buffers t
      dired-free-space t
      dired-create-destination-dirs 'ask)

(setopt dired-recursive-copies 'always
	dired-recursive-deletes 'always)

;;eglot
(add-hook 'prog-mode-hook #'eglot-ensure)
(setq eldoc-echo-area-use-multiline-p nil
      eldoc-echo-area-display-truncation-message nil
      eglot-events-buffer-size 0
      eglot-extend-to-xref t)
;;(add-hook 'eglot--managed-mode-hook (lambda () (flymake-mode -1)))
(add-hook 'eglot-managed-mode-hook (lambda () (eglot-inlay-hints-mode -1)))
(setq eglot-report-progress nil)

(setq xref-prompt-for-identifier t)

;;fonts
;; (set-face-attribute 'default nil
;; 		    :family "Iosevka Fixed"
;; 		    ;;:foundry "outline"
;; 		    :foundry "UKWN"
;; 		    ;;:slant 'italic
;; 		    ;;:slant 'oblique
;; 		    :weight 'regular
;; 		    :height 185
;; ;;		    :height 138
;; 		    :width 'normal)

;;(set-frame-font "Iosevka Oblique 17" nil t)

(set-face-attribute 'default nil :family "Aporetic Sans Mono" :height 200 )



;; (add-to-list 'default-frame-alist '(width . 150))
;; (add-to-list 'default-frame-alist '(height . 50))

;;repeat-mode
(repeat-mode)

;;subword
(subword-mode)

;;follow-mode
;;(follow-mode 1)

(save-place-mode)
(setq-default fill-column 80)

(add-hook 'minibuffer-mode #'savehist-mode)
(add-hook 'prog-mode-hook #'hs-minor-mode)

(setq compile-command "cmake --fresh -G Ninja -D CMAKE_EXPORT_COMPILE_COMMANDS=ON -S . -B build && cmake --build build --parallel")

(setq-default cursor-type 'bar) ; 设置光标为竖线
;;(setq-default cursor-type 'box) ; 设置光标为方块

;; copy selected text
(setq mouse-drag-copy-region t)
;; enable visual feedback on selections
(setq transient-mark-mode t)
;; show the boundaries of the file
(setq-default indicate-buffer-boundaries 'right)

;; default to better frame titles
;; (setq frame-title-format
;;       (concat  "%b - emacs@" system-name))

;; show trailing spaces and empty lines
;; (setq-default highlight-tabs t)
;; (setq-default show-trailing-whitespace t)
;; (setq-default indicate-empty-lines t)

;; turn on interactive do
;; (ido-mode t)
;; (setq ido-enable-flex-matching t)
;; (setq ido-everywhere t)

;;(setq package-install-upgrade-built-in t)

;; (setq emojify-display-style 'unicode)
;; (setq emojify-emoji-styles '(unicode))

;; (setq w32-non-USB-fonts '((emoji Noto\ Emoji)))

;;(setq package-install-upgrade-built-in t)

;; The nano style for truncated long lines.
;; (setq auto-hscroll-mode #'current-line)

;; gud
;; (add-hook 'gud  #'gud-tooltip-mode)

;;(server-start)

(setq-default xref-search-program 'ugrep)


(setq minibuffer-visible-completions t)

;; SQL
(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (toggle-truncate-lines t)))

;; which-key
(which-key-mode)



;; Enable Completion Preview mode in code buffers
(add-hook 'prog-mode-hook #'completion-preview-mode)
;; also in text buffers
(add-hook 'text-mode-hook #'completion-preview-mode)
;; and in \\[shell] and friends
(with-eval-after-load 'comint
  (add-hook 'comint-mode-hook #'completion-preview-mode))

(with-eval-after-load 'completion-preview
  ;; Show the preview already after two symbol characters
  (setq completion-preview-minimum-symbol-length 2)

  ;; Non-standard commands to that should show the preview:

  ;; Org mode has a custom `self-insert-command'
  (push 'org-self-insert-command completion-preview-commands)
  ;; Paredit has a custom `delete-backward-char' command
  (push 'paredit-backward-delete completion-preview-commands)

  ;; Bindings that take effect when the preview is shown:

  ;; ;; Cycle the completion candidate that the preview shows
  ;; (keymap-set completion-preview-active-mode-map "M-n" #'completion-preview-next-candidate)
  ;; (keymap-set completion-preview-active-mode-map "M-p" #'completion-preview-prev-candidate)
  ;; ;; Convenient alternative to C-i after typing one of the above
  ;; (keymap-set completion-preview-active-mode-map "M-i" #'completion-preview-insert)
  )


;;
(save-some-buffers t)





(provide 'init-builtin)
;;; init-builtin.el ends here
;; Local Variables:
;; byte-compile-warnings: (not free-vars unresolved)
;; End:
