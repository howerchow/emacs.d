;;; Code:

;; ;;doom-themes
;; (unless (package-installed-p 'doom-themes)
;;   (package-install 'doom-themes))
;; (require 'doom-themes)
;; ;;(load-theme 'doom-monokai-machine t)
;; ;;(load-theme 'doom-zenburn t)
;; ;;(load-theme 'doom-material t)
;; ;;(load-theme 'doom-nord t)
;; (load-theme 'doom-monokai-classic t)


;; modus-themes
;; (unless (package-installed-p 'modus-themes)
;;   (package-install 'modus-themes))
;; (require 'modus-themes)

;;(load-theme 'modus-operandi :no-confirm)
;;(load-theme 'modus-operandi-tinted :no-confirm)
;;(load-theme 'modus-vivendi-tritanopia :no-confirm)

;; Add all your customizations prior to loading the themes
;; (setq
;;  modus-themes-completions nil
;;  modus-themes-bold-constructs t
;;  modus-themes-italic-constructs t
;;  modus-themes-success-deuteranopia t
;;  modus-themes-mixed-fonts t
;;  modus-themes-intense-markup t
;;  modus-themes-tabs-accented t
;;  modus-themes-org-blocks '(tinted-background)
;;  modus-themes-variable-pitch-ui nil
;;  modus-themes-variable-pitch-headings nil

;;  modus-themes-fringes nil ; {nil,'subtle,'intense}

;;  ;; fonts for headings
;;  modus-themes-headings
;;  '((1 . (background overline rainbow 1.10))
;;    (2 . (overline rainbow 1.05))
;;    (3 . (rainbow 1.1))
;;    (4 . (rainbow no-bold 1.0))
;;    (t . (monochrome no-bold 1.0)))
;;  ;; scales up the headings
;;  modus-themes-scale-headings t)


;; ef-themes
(unless (package-installed-p 'ef-themes)
  (package-install 'ef-themes))
(require 'ef-themes)
(load-theme 'ef-bio :no-confirm)
;;(load-theme 'ef-trio-dark :no-confirm)


(provide 'init-themes)

;;; init-org.el ends here
