;;; Package --- init.el
;;; Commentary:
;;; Code:

(add-to-list 'load-path (expand-file-name (concat user-emacs-directory "lisp")))

;; Adjust garbage collection thresholds during startup, and thereafter
;; (let ((normal-gc-cons-threshold (* 20 1024 1024))
;;       (init-gc-cons-threshold (* 128 1024 1024)))
;;   (setq gc-cons-threshold init-gc-cons-threshold)
;;   (add-hook 'emacs-startup-hook
;;             (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(require 'init-elpa)
(require 'init-themes)
(require 'init-builtin)
(require 'init-package)
(require 'init-org)
(require 'init-latex)
;;(require 'init-treemacs)
;;(require 'init-citre)

;; Variables configured via the interactive 'customize' interface
(when (file-exists-p custom-file)
  (load custom-file))

;;; init.el ends here
